import { Component } from '@angular/core';

import { InventarioPage } from '../inventario/inventario';
import { MundoPage } from '../mundo/mundo';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tabMundo = MundoPage;
  tabInventario = InventarioPage;

  constructor() {

  }
}
