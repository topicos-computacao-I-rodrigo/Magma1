import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { SoniPage } from '../soni/soni';

@Component({
  selector: 'page-mundo',
  templateUrl: 'mundo.html'
})
export class MundoPage {

  constructor(public navCtrl: NavController) {

  }

  goToSoni() {
    this.navCtrl.push(SoniPage);
  }

}
