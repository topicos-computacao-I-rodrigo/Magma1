import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { MundoPage } from '../pages/mundo/mundo';
import { TabsPage } from '../pages/tabs/tabs';
import { InventarioPage } from '../pages/inventario/inventario';
import { SoniPage } from '../pages/soni/soni';

@NgModule({
  declarations: [
    MyApp,
    MundoPage,
    InventarioPage,
    SoniPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MundoPage,
    InventarioPage,
    SoniPage,
    TabsPage
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
